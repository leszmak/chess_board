#Chess Board
Chess_Board is a project that allows users to play chess on a chessboard connected to a Raspberry Pi. The chessboard provides hints for possible moves. Thanks to Chess_Board_Mobile, both players don't have to stay next to the chessboard. One can play from their mobile device.

#Chess_Board Mobile
This folder contains an application ready to run on a mobile device. In the future, I will connect the mobile app with the Raspberry Pi.

#Chess_Board_Raspberry
This folder contains everything necessary for the Raspberry Pi: the circuit and the program to run.

Work in progress since 2023 and is barely developed...


