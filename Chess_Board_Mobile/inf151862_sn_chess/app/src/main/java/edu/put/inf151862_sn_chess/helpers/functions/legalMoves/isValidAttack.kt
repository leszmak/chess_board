package edu.put.inf151862_sn_chess.helpers.functions.legalMoves

fun isValidAttack(x: Int, y: Int, chessBoard: List<MutableList<Int>>, whiteOnMove: Boolean): Boolean {
    if (x !in 0..7 || y !in 0..7) return false
    val targetPiece = chessBoard[y][x]
    return if (whiteOnMove) targetPiece < 0 else targetPiece > 0
}