package edu.put.inf151862_sn_chess

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.room.Room
import edu.put.inf151862_sn_chess.helpers.RoomDb
import edu.put.inf151862_sn_chess.navigation.Navigation
import edu.put.inf151862_sn_chess.ui.theme.Inf151862_sn_chessTheme

class MainActivity : ComponentActivity() {
    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            RoomDb::class.java,
            "chessGame.db"
        ).build()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Inf151862_sn_chessTheme {
                Navigation(db)
            }
        }

    }
}