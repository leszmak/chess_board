package edu.put.inf151862_sn_chess.logic

import edu.put.inf151862_sn_chess.helpers.functions.legalMoves.isValidAttack
import edu.put.inf151862_sn_chess.helpers.functions.legalMoves.isValidMove
import edu.put.inf151862_sn_chess.helpers.functions.legalMoves.isValidPosition
import edu.put.inf151862_sn_chess.navigation.StateManager

fun queen(stateManager: StateManager) {
    val x = stateManager.chosenPiece.value[0]
    val y = stateManager.chosenPiece.value[1]
    val chessBoard = stateManager.chessBoard.value

    var legalMoves = arrayOf(arrayOf<Int?>(null, null)) // Initialize legal moves array
    var legalAttacks = arrayOf(arrayOf<Int?>(null, null)) // Initialize legal attacks array

    if (x == null || y == null) return // Exit if chosen piece position is null

    // Possible queen moves directions (combining rook and bishop moves)
    val directions = listOf(
        Pair(1, 0), Pair(-1, 0), Pair(0, 1), Pair(0, -1), // Rook-like moves
        Pair(1, 1), Pair(1, -1), Pair(-1, 1), Pair(-1, -1) // Bishop-like moves
    )

    for (direction in directions) {
        var newX = x
        var newY = y

        while (true) {
            newX += direction.first
            newY += direction.second

            if (!isValidPosition(newX, newY)) {
                break
            }
            if (isValidAttack(newX, newY, chessBoard, stateManager.whiteOnMove.value)) {
                legalAttacks += arrayOf(newX, newY)
                break
            }

            if (isValidMove(newX, newY, chessBoard)) {
                legalMoves += arrayOf(newX, newY)
            }
            else{
                break
            }
        }
    }

    stateManager.legalMoves.value = legalMoves
    stateManager.legalAttacks.value = legalAttacks
}
