package edu.put.inf151862_sn_chess.logic

import edu.put.inf151862_sn_chess.helpers.functions.legalMoves.isValidAttack
import edu.put.inf151862_sn_chess.helpers.functions.legalMoves.isValidMove
import edu.put.inf151862_sn_chess.navigation.StateManager

fun king(stateManager: StateManager) {
    val x = stateManager.chosenPiece.value[0]
    val y = stateManager.chosenPiece.value[1]
    val chessBoard = stateManager.chessBoard.value

    var res = arrayOf(arrayOf<Int?>(null, null)) // Initialize legal moves array
    var res2 = arrayOf(arrayOf<Int?>(null, null)) // Initialize legal attacks array

    if (x == null || y == null) return // Exit if chosen piece position is null

    // Possible king moves (x, y) offsets
    val kingMoves = listOf(
        Pair(1, 0), Pair(1, 1), Pair(0, 1), Pair(-1, 1),
        Pair(-1, 0), Pair(-1, -1), Pair(0, -1), Pair(1, -1)
    )

    for (move in kingMoves) {
        val newX = x + move.first
        val newY = y + move.second

        if (isValidMove(newX, newY, chessBoard)) {
            res += arrayOf(newX, newY)
        }
        if (isValidAttack(newX, newY, chessBoard, stateManager.whiteOnMove.value)) {
            res2 += arrayOf(newX, newY)
        }
    }

    stateManager.legalMoves.value = res
    stateManager.legalAttacks.value = res2
}