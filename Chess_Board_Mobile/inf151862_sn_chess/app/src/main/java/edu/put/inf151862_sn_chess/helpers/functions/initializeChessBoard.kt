package edu.put.inf151862_sn_chess.helpers.functions

import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.toMutableStateList
import edu.put.inf151862_sn_chess.navigation.StateManager

fun initializeChessBoard(stateManager: StateManager) {

    stateManager.chosenPiece.value = arrayOf<Int?>(null, null)
    stateManager.legalMoves.value =  arrayOf(arrayOf<Int?>(null, null))
    stateManager.legalAttacks.value = arrayOf(arrayOf<Int?>(null, null))
    stateManager.whiteOnMove.value = true

    val initialBoard: List<MutableList<Int>> = listOf(
        mutableListOf(-4, -2, -3, -5, -6, -3, -2, -4),
        mutableListOf(-1, -1, -1, -1, -1, -1, -1, -1),
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0),
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0),
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0),
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0),
        mutableListOf(1, 1, 1, 1, 1, 1, 1, 1),
        mutableListOf(4, 2, 3, 5, 6, 3, 2, 4)
    )

    // Update the stateManager's chessBoard with the initialBoard values
    initialBoard.forEachIndexed { rowIndex, row ->
        row.forEachIndexed { colIndex, value ->
            stateManager.chessBoard.value[rowIndex][colIndex] = value
        }
    }
}