package edu.put.inf151862_sn_chess.helpers.game_page

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import edu.put.inf151862_sn_chess.logic.bishop
import edu.put.inf151862_sn_chess.logic.king
import edu.put.inf151862_sn_chess.logic.knight
import edu.put.inf151862_sn_chess.logic.pawn
import edu.put.inf151862_sn_chess.logic.queen
import edu.put.inf151862_sn_chess.logic.rook
import edu.put.inf151862_sn_chess.navigation.StateManager
import kotlin.math.abs

@Composable
fun ChessPiece(
    painter: Painter,
    x: Int,
    y: Int,
    boxSize: Dp,
    stateManager: StateManager
) {
    val pieceSize = boxSize / 8
    Image(
        painter = painter,
        contentDescription = "Chess Piece",
        modifier = Modifier
            .size(pieceSize * 0.7f)
            .offset(
                x = (x * pieceSize.value + pieceSize.value * 0.15f).dp,
                y = (y * pieceSize.value + pieceSize.value * 0.15f).dp
            )
            .clickable {
                val piece = stateManager.chessBoard.value[y][x]
                val whiteOnMove = stateManager.whiteOnMove.value

                if ((piece > 0) == whiteOnMove) {
                    stateManager.chosenPiece.value = arrayOf(x, y)

                    when (abs(piece)) {
                        1 -> pawn(stateManager)
                        2 -> knight(stateManager)
                        3 -> bishop(stateManager)
                        4 -> rook(stateManager)
                        5 -> queen(stateManager)
                        6 -> king(stateManager)
                    }
                }
                else {
                    stateManager.legalAttacks.value.forEach { attack ->
                        val (currX, currY) = attack
                        if (currX == x && currY == y) {
                            val chosenPiece = stateManager.chosenPiece.value
                            val chosenX = chosenPiece[0]
                            val chosenY = chosenPiece[1]

                            if (chosenX != null && chosenY != null) {
                                stateManager.chessBoard.value[y][x] = stateManager.chessBoard.value[chosenY][chosenX]
                                stateManager.chessBoard.value[chosenY][chosenX] = 0
                            }

                            stateManager.legalMoves.value = arrayOf(arrayOf<Int?>(null, null))
                            stateManager.legalAttacks.value = arrayOf(arrayOf<Int?>(null, null))

                            stateManager.chosenPiece.value = arrayOf(null, null)

                            stateManager.whiteOnMove.value = !stateManager.whiteOnMove.value                        }
                    }
                }
            },
        contentScale = ContentScale.Fit
    )
}
