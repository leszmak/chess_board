package edu.put.inf151862_sn_chess.helpers.functions.legalMoves

fun isValidPosition(x: Int, y: Int): Boolean {
    return x in 0..7 && y in 0..7
}