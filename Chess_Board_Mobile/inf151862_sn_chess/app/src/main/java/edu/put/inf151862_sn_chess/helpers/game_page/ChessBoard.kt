package edu.put.inf151862_sn_chess.helpers.game_page

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.navigation.NavController
import edu.put.inf151862_sn_chess.R
import edu.put.inf151862_sn_chess.navigation.StateManager

@Composable
fun ChessBoard(navController: NavController, stateManager: StateManager) {
    BoxWithConstraints(modifier = Modifier) {
        val boxSize = if (maxWidth < maxHeight) maxWidth else maxHeight

        Box(modifier = Modifier.size(boxSize)) {
            Image(
                painter = painterResource(id = R.drawable.chess_board),
                contentDescription = "Chess Board",
                modifier = Modifier.size(boxSize),
                contentScale = ContentScale.FillBounds
            )
            ChosenPiece(stateManager = stateManager, boxSize = boxSize)
            LegalMoves(stateManager = stateManager, boxSize = boxSize)
            LegalAttacks(stateManager = stateManager, boxSize = boxSize)
            PlacePieces(stateManager = stateManager, boxSize = boxSize)
        }
    }
}