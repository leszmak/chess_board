package edu.put.inf151862_sn_chess.pages

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectHorizontalDragGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import edu.put.inf151862_sn_chess.R
import edu.put.inf151862_sn_chess.navigation.NavBar
import edu.put.inf151862_sn_chess.navigation.Screen
import edu.put.inf151862_sn_chess.navigation.StateManager

@Composable
fun TabletMainPage(navController: NavController, stateManager: StateManager) {
    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        if (maxWidth < maxHeight) navController.navigate(Screen.MainPage.route)
        val boxSize = maxHeight
        val imgSize = boxSize / 5

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(stateManager.brightBackgroundColor.value).pointerInput(Unit) {
                    detectHorizontalDragGestures(
                        onHorizontalDrag = { change, dragAmount ->
                            when {
                                dragAmount < 0 -> {
                                    navController.navigate(Screen.TabletGamePage.route)
                                }
                            }
                        }
                    )
                }
        ) {

            Row(modifier = Modifier.fillMaxWidth().padding(top = 16.dp),
                horizontalArrangement = Arrangement.Center) {
                Text(
                    text = "Szachy",
                    color = stateManager.fontColor.value,
                    style = MaterialTheme.typography.titleLarge.copy(fontSize = 32.sp),
                )
            }

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                horizontalArrangement = Arrangement.SpaceAround,
                verticalAlignment = Alignment.CenterVertically
            ) {
                val pieces = listOf(
                    Pair(R.drawable.black_king, R.drawable.white_king),
                    Pair(R.drawable.black_queen, R.drawable.white_queen),
                    Pair(R.drawable.black_rook, R.drawable.white_rook),
                    Pair(R.drawable.black_bishop, R.drawable.white_bishop),
                    Pair(R.drawable.black_knight, R.drawable.white_knight),
                    Pair(R.drawable.black_pawn, R.drawable.white_pawn)
                )

                pieces.forEach { (black, white) ->
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier
                    ) {
                        Image(
                            painter = painterResource(id = black),
                            contentDescription = null,
                            modifier = Modifier.size(imgSize)
                        )
                        Image(
                            painter = painterResource(id = white),
                            contentDescription = null,
                            modifier = Modifier.size(imgSize)
                        )
                    }
                }
            }

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Szymon Nowak 151862",
                    style = MaterialTheme.typography.bodyLarge,
                    color = stateManager.fontColor.value,
                    modifier = Modifier
                )
            }

            NavBar(navController = navController)
        }
    }
}