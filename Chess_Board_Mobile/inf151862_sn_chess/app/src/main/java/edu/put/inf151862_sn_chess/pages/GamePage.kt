package edu.put.inf151862_sn_chess.pages

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectHorizontalDragGestures
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import edu.put.inf151862_sn_chess.helpers.game_page.ChessBoard
import edu.put.inf151862_sn_chess.helpers.game_page.Menu
import edu.put.inf151862_sn_chess.navigation.NavBar
import edu.put.inf151862_sn_chess.navigation.Screen
import edu.put.inf151862_sn_chess.navigation.StateManager


@Composable
fun GamePage(navController: NavController, stateManager: StateManager) {
    val scrollState = rememberScrollState()
    BoxWithConstraints(modifier = Modifier) {
        if (maxWidth > maxHeight) navController.navigate(Screen.TabletGamePage.route)
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(stateManager.brightBackgroundColor.value)
                .pointerInput(Unit) {
                    detectHorizontalDragGestures(
                        onHorizontalDrag = { change, dragAmount ->
                            when {
                                dragAmount > 0 -> {
                                    navController.navigate(Screen.MainPage.route)
                                }
                                dragAmount < 0 -> {
                                    navController.navigate(Screen.Settings.route)
                                }
                            }
                        }
                    )
                }

        ) {
            Column(modifier = Modifier.weight(1f).verticalScroll(scrollState).padding(8.dp)) {
                Row() {
                    Text(
                        text = "Przeciwnik",
                        style = MaterialTheme.typography.titleLarge,
                        color = stateManager.fontColor.value,
                        modifier = Modifier
                    )
                    if (!stateManager.whiteOnMove.value) {
                        Text(
                            text = " - jego ruch",
                            style = MaterialTheme.typography.titleLarge,
                            color = stateManager.fontColor.value,
                            modifier = Modifier
                        )
                    }
                }

                Spacer(modifier = Modifier.height(8.dp))

                Column(
                    modifier = Modifier
                        .background(color = stateManager.mediumBackgroundColor.value)
                ) {
                    ChessBoard(navController = navController, stateManager = stateManager)
                }

                Row() {
                    Text(
                        text = "Ty",
                        style = MaterialTheme.typography.titleLarge,
                        color = stateManager.fontColor.value,
                        modifier = Modifier
                    )
                    if (stateManager.whiteOnMove.value) {
                        Text(
                            text = " - twój ruch",
                            style = MaterialTheme.typography.titleLarge,
                            color = stateManager.fontColor.value,
                            modifier = Modifier
                        )
                    }
                }

                Spacer(modifier = Modifier.height(8.dp))
                Menu(stateManager)
            }
            NavBar(navController = navController)
        }
    }
}
