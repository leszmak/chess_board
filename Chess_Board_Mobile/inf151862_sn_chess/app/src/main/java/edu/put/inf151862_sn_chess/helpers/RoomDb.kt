package edu.put.inf151862_sn_chess.helpers

import androidx.room.Dao
import androidx.room.Database
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.RoomDatabase
import androidx.room.Upsert

@Database(
    entities = [MyDbGame::class],
    version = 1,
    autoMigrations = []
)

abstract class RoomDb : RoomDatabase() {
    abstract val myDbItemDao: MyDbGameDao

}

@Entity(tableName = "my_db_table")
data class MyDbGame(
    var board: String,
    var whiteOnMove: Boolean,
    @PrimaryKey
    val id: Int = 1
);

@Dao
interface MyDbGameDao {
    @Upsert
    suspend fun upsertMyDbItem(itemToUpsert: MyDbGame)

    @Delete
    suspend fun deleteDashboardItem(itemToDelete: MyDbGame)

    @Query("DELETE FROM my_db_table")
    suspend fun clear()

    @Query("SELECT board FROM my_db_table where id = 1")
    suspend fun getMyDbBoard() : String

    @Query("SELECT whiteOnMove FROM my_db_table where id = 1")
    suspend fun getMyDbWhiteOnMMove() : Boolean

}