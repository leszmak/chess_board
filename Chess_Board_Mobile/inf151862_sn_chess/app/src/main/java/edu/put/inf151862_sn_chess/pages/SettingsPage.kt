package edu.put.inf151862_sn_chess.pages

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectHorizontalDragGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import edu.put.inf151862_sn_chess.helpers.DarkModeSwitch
import edu.put.inf151862_sn_chess.navigation.NavBar
import edu.put.inf151862_sn_chess.navigation.Screen
import edu.put.inf151862_sn_chess.navigation.StateManager

@Composable
fun SettingsPage(navController: NavController, stateManager: StateManager) {
    BoxWithConstraints(modifier = Modifier) {
        if (maxWidth > maxHeight) navController.navigate(Screen.TabletSettingsPage.route)

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(stateManager.brightBackgroundColor.value).pointerInput(Unit) {
                    detectHorizontalDragGestures(
                        onHorizontalDrag = { change, dragAmount ->
                            when {
                                dragAmount > 0 -> {
                                    navController.navigate(Screen.GamePage.route)
                                }
                            }
                        }
                    )
                }
        ) {
            Column(
                modifier = Modifier
                    .weight(1f)
                    .padding(16.dp),
                verticalArrangement = Arrangement.spacedBy(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                // Title
                Text(
                    "Ustawienia",
                    style = MaterialTheme.typography.titleLarge.copy(color = stateManager.fontColor.value)
                )

                // Dark Mode Toggle
                Card(
                    shape = RoundedCornerShape(10.dp),
                    modifier = Modifier.fillMaxWidth(),
                    colors = CardDefaults.cardColors(containerColor = stateManager.mediumBackgroundColor.value)
                ) {
                    Row(
                        modifier = Modifier
                            .padding(16.dp)
                            .fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Icon(
                                Icons.Filled.Star,
                                contentDescription = "Tryb nocny",
                                tint = Color(0xFFFFD700)
                            )
                            Spacer(modifier = Modifier.width(8.dp))
                            Text("Tryb nocny", color = stateManager.fontColor.value)
                        }
                        DarkModeSwitch(stateManager)
                    }
                }

                // Navigate to Raspberry Pi Connection
                Button(
                    onClick = {  },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 16.dp)
                        .height(50.dp),
                    shape = RoundedCornerShape(10.dp),
                ) {
                    Text(
                        "Połącz się z Raspberry Pi",
                        color = Color.White,
                        fontWeight = FontWeight.Bold
                    )
                }
            }
            NavBar(navController = navController)
        }
    }
}