package edu.put.inf151862_sn_chess.pages

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.painterResource
import androidx.navigation.NavController
import edu.put.inf151862_sn_chess.R
import edu.put.inf151862_sn_chess.navigation.Screen
import kotlinx.coroutines.delay


@Composable
fun Animation(navController: NavController) {
    var animationState by remember { mutableStateOf(AnimationState.INITIAL) }
    var screenWidth = 0f

    val blackOffsets =  remember { List(6) { Animatable(-200f) }}
    val whiteOffsets =  remember { List(6) { Animatable(200f) }}
    BoxWithConstraints(modifier = Modifier) {
        if (maxWidth > maxHeight) navController.navigate(Screen.TabletAnimation.route)
        val imgSize =  maxWidth/4
        LaunchedEffect(animationState) {
                when (animationState) {
                    AnimationState.INITIAL -> {
                        blackOffsets.forEach { it.snapTo(-screenWidth) }
                        whiteOffsets.forEach { it.snapTo(screenWidth) }
                        animationState = AnimationState.MOVE_KING
                    }

                    AnimationState.MOVE_KING -> {
                        animatePieces(blackOffsets[0], whiteOffsets[0], screenWidth)
                        animationState = AnimationState.MOVE_QUEEN
                    }

                    AnimationState.MOVE_QUEEN -> {
                        animatePieces(blackOffsets[1], whiteOffsets[1], screenWidth)
                        animationState = AnimationState.MOVE_ROOK
                    }

                    AnimationState.MOVE_ROOK -> {
                        animatePieces(blackOffsets[2], whiteOffsets[2], screenWidth)
                        animationState = AnimationState.MOVE_BISHOP
                    }

                    AnimationState.MOVE_BISHOP -> {
                        animatePieces(blackOffsets[3], whiteOffsets[3], screenWidth)
                        animationState = AnimationState.MOVE_KNIGHT
                    }

                    AnimationState.MOVE_KNIGHT -> {
                        animatePieces(blackOffsets[4], whiteOffsets[4], screenWidth)
                        animationState = AnimationState.MOVE_PAWN
                    }

                    AnimationState.MOVE_PAWN -> {
                        animatePieces(blackOffsets[5], whiteOffsets[5], screenWidth)
                        animationState = AnimationState.WAIT
                    }

                    AnimationState.WAIT -> {
                        delay(1000)
                        navController.navigate(Screen.MainPage.route)
                    }
            }
        }

        BoxWithConstraints(
            modifier = Modifier.fillMaxSize()
        ) {
            screenWidth = maxWidth.value

            val blackPieces = listOf(
                R.drawable.black_king,
                R.drawable.black_queen,
                R.drawable.black_rook,
                R.drawable.black_bishop,
                R.drawable.black_knight,
                R.drawable.black_pawn
            )

            val whitePieces = listOf(
                R.drawable.white_king,
                R.drawable.white_queen,
                R.drawable.white_rook,
                R.drawable.white_bishop,
                R.drawable.white_knight,
                R.drawable.white_pawn
            )

            Column(
                horizontalAlignment = Alignment.Start,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                blackPieces.forEachIndexed { index, piece ->
                    Image(
                        painter = painterResource(id = piece),
                        contentDescription = null,
                        modifier = Modifier
                            .size(imgSize)
                            .graphicsLayer(translationX = blackOffsets[index].value)
                    )
                }
            }

            Column(
                horizontalAlignment = Alignment.End,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                whitePieces.forEachIndexed { index, piece ->
                    Image(
                        painter = painterResource(id = piece),
                        contentDescription = null,
                        modifier = Modifier
                            .size(imgSize)
                            .graphicsLayer(translationX = whiteOffsets[index].value)
                    )
                }
            }
        }
    }
}

suspend fun animatePieces(
    blackPiece: Animatable<Float, *>,
    whitePiece: Animatable<Float, *>,
    screenWidth: Float
) {
    blackPiece.animateTo(
        targetValue = screenWidth * 0.25f,
        animationSpec = tween(durationMillis = 250)
    )
    whitePiece.animateTo(
        targetValue = -screenWidth * 0.25f,
        animationSpec = tween(durationMillis = 250)
    )
}

enum class AnimationState {
    INITIAL,
    MOVE_KING,
    MOVE_QUEEN,
    MOVE_ROOK,
    MOVE_BISHOP,
    MOVE_KNIGHT,
    MOVE_PAWN,
    WAIT
}