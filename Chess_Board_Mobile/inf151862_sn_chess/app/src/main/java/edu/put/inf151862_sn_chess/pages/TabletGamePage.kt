package edu.put.inf151862_sn_chess.pages

import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectHorizontalDragGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import edu.put.inf151862_sn_chess.helpers.game_page.ChessBoard
import edu.put.inf151862_sn_chess.helpers.game_page.Menu
import edu.put.inf151862_sn_chess.navigation.NavBar
import edu.put.inf151862_sn_chess.navigation.Screen
import edu.put.inf151862_sn_chess.navigation.StateManager

@Composable
fun TabletGamePage(navController: NavController, stateManager: StateManager) {
    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        if (maxWidth < maxHeight) navController.navigate(Screen.GamePage.route)
        val boxSize = maxHeight
        val imgSize = boxSize / 5
        Row(
            modifier = Modifier
                .fillMaxSize()
                .background(stateManager.brightBackgroundColor.value).pointerInput(Unit) {
                    detectHorizontalDragGestures(
                        onHorizontalDrag = { change, dragAmount ->
                            when {
                                dragAmount > 0 -> {
                                    navController.navigate(Screen.TabletMainPage.route)
                                }
                                dragAmount < 0 -> {
                                    navController.navigate(Screen.TabletSettingsPage.route)
                                }
                            }
                        }
                    )
                }
        ) {
            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxHeight()
                    .padding(16.dp)
            ) {
                Row {
                    Text(
                        text = "Przeciwnik",
                        style = MaterialTheme.typography.titleLarge,
                        color = stateManager.fontColor.value,
                        modifier = Modifier
                    )
                    if (!stateManager.whiteOnMove.value) {
                        Text(
                            text = " - jego ruch",
                            style = MaterialTheme.typography.titleLarge,
                            color = stateManager.fontColor.value,
                            modifier = Modifier
                        )
                    }
                }

                Spacer(modifier = Modifier.height(8.dp))

                Column(
                    modifier = Modifier
                        .background(color = stateManager.mediumBackgroundColor.value)
                        .weight(1f)
                        .fillMaxWidth(),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    ChessBoard(navController = navController, stateManager = stateManager)
                }

                Row {
                    Text(
                        text = "Ty",
                        style = MaterialTheme.typography.titleLarge,
                        color = stateManager.fontColor.value,
                        modifier = Modifier
                    )
                    if (stateManager.whiteOnMove.value) {
                        Text(
                            text = " - twój ruch",
                            style = MaterialTheme.typography.titleLarge,
                            color = stateManager.fontColor.value,
                            modifier = Modifier
                        )
                    }
                }

                Spacer(modifier = Modifier.height(8.dp))
            }

            Column(
                modifier = Modifier
                    .weight(1f)
                    .fillMaxHeight()
                    .padding(16.dp)
            ) {
                Menu(stateManager)

                Spacer(modifier = Modifier.weight(1f))

                NavBar(navController = navController)
            }
        }
    }
}