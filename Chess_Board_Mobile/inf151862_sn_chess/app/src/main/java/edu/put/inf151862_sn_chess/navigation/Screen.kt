package edu.put.inf151862_sn_chess.navigation

sealed class Screen(val route: String) {
    object GamePage: Screen("gamepage")
    object Animation: Screen("animation")
    object Settings: Screen("settings")
    object MainPage: Screen("mainpage")
    object TabletMainPage: Screen("tabletmainpage")
    object TabletGamePage: Screen("tabletgamepage")
    object TabletSettingsPage: Screen("tabletsettingspage")
    object TabletAnimation: Screen("tabletanimation")
}