package edu.put.inf151862_sn_chess.helpers.game_page

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import edu.put.inf151862_sn_chess.R
import edu.put.inf151862_sn_chess.navigation.StateManager

@Composable
fun PlacePieces(stateManager: StateManager, boxSize: Dp) {
    stateManager.chessBoard.value.forEachIndexed { rowIndex, row ->
        row.forEachIndexed { colIndex, piece ->
            val pieceRes = when (piece) {
                1 -> R.drawable.white_pawn
                2 -> R.drawable.white_knight
                3 -> R.drawable.white_bishop
                4 -> R.drawable.white_rook
                5 -> R.drawable.white_queen
                6 -> R.drawable.white_king
                -1 -> R.drawable.black_pawn
                -2 -> R.drawable.black_knight
                -3 -> R.drawable.black_bishop
                -4 -> R.drawable.black_rook
                -5 -> R.drawable.black_queen
                -6 -> R.drawable.black_king
                else -> null
            }

            pieceRes?.let {
                ChessPiece(
                    painter = painterResource(id = it),
                    x = colIndex,
                    y = rowIndex,
                    boxSize = boxSize,
                    stateManager = stateManager
                )
            }
        }
    }
}