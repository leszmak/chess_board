package edu.put.inf151862_sn_chess.helpers.game_page

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import edu.put.inf151862_sn_chess.helpers.functions.initializeChessBoard
import edu.put.inf151862_sn_chess.navigation.StateManager
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import edu.put.inf151862_sn_chess.helpers.functions.loadGame
import edu.put.inf151862_sn_chess.helpers.functions.saveGame

@Composable
fun Menu(stateManager: StateManager) {
    Column(
        modifier = Modifier
            .fillMaxHeight(0.8f)
            .padding(16.dp)
            .background(color = stateManager.mediumBackgroundColor.value, shape = RoundedCornerShape(8.dp))
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            text = "Menu",
            style = MaterialTheme.typography.titleLarge,
            color = stateManager.fontColor.value,
            modifier = Modifier.padding(bottom = 16.dp)
        )

        Button(
            onClick = { initializeChessBoard(stateManager) },
            modifier = Modifier
                .fillMaxWidth(),
        colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF6200EE))
        ) {
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Nowa gra")
        }

        Button(
            onClick = { saveGame(stateManager) },
            modifier = Modifier
                .fillMaxWidth(),
            colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF6200EE))
        ) {
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Zapisz stan gry")
        }

        Button(
            onClick = { loadGame(stateManager) },
            modifier = Modifier
                .fillMaxWidth(),
            colors = ButtonDefaults.buttonColors(containerColor = Color(0xFF6200EE))
        ) {
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Wczytaj grę")
        }
    }
}