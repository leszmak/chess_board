package edu.put.inf151862_sn_chess.logic

import edu.put.inf151862_sn_chess.helpers.functions.legalMoves.isValidMove
import edu.put.inf151862_sn_chess.helpers.functions.legalMoves.isValidAttack
import edu.put.inf151862_sn_chess.helpers.functions.legalMoves.willKingBeAttacked
import edu.put.inf151862_sn_chess.navigation.StateManager

fun pawn(stateManager: StateManager) {
    val x = stateManager.chosenPiece.value[0]
    val y = stateManager.chosenPiece.value[1]
    val chessBoard = stateManager.chessBoard.value

    var legalMoves = arrayOf(arrayOf<Int?>(null, null)) // Clear existing legal moves
    var legalAttacks = arrayOf(arrayOf<Int?>(null, null)) // Clear existing legal attacks

    if (x == null || y == null) return // Exit if chosen piece position is null

    val direction = if (chessBoard[y][x] > 0) -1 else 1 // Determine direction based on pawn color

    // Check single step move
    if (isValidMove(x, y + direction, chessBoard)) {
        legalMoves += arrayOf<Int?>(x, y + direction)

        // Check double step move from the starting position
        val startRow = if (direction == -1) 6 else 1
        if (y == startRow && isValidMove(x, y + 2 * direction, chessBoard) ) {
            legalMoves += arrayOf<Int?>(x, y + 2 * direction)
        }
    }

    // Check attack moves
    val attackPositions = listOf(Pair(x - 1, y + direction), Pair(x + 1, y + direction))
    for (pos in attackPositions) {
        val (attackX, attackY) = pos
        if (isValidAttack(attackX, attackY, chessBoard, stateManager.whiteOnMove.value)) {
            legalAttacks += arrayOf(attackX, attackY)
        }
    }

    stateManager.legalMoves.value = legalMoves
    stateManager.legalAttacks.value = legalAttacks
}