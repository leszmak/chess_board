package edu.put.inf151862_sn_chess.pages

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectHorizontalDragGestures
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import edu.put.inf151862_sn_chess.R
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import edu.put.inf151862_sn_chess.navigation.NavBar
import edu.put.inf151862_sn_chess.navigation.Screen
import edu.put.inf151862_sn_chess.navigation.StateManager

@Composable
fun MainPage(navController: NavController, stateManager: StateManager) {
    BoxWithConstraints(modifier = Modifier) {
        if (maxWidth > maxHeight) navController.navigate(Screen.TabletMainPage.route)
        val boxSize =  maxWidth
        val imgSize = boxSize/6
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(stateManager.brightBackgroundColor.value).pointerInput(Unit) {
                    detectHorizontalDragGestures(
                        onHorizontalDrag = { change, dragAmount ->
                            when {
                                dragAmount < 0 -> {
                                    navController.navigate(Screen.GamePage.route)
                                }
                            }
                        }
                    )
                },
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Column(
                modifier = Modifier
                    .weight(1f)
                    .padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.black_king), // Replace with your image resource
                        contentDescription = "White King",
                        modifier = Modifier.size(imgSize),
                        contentScale = ContentScale.Crop
                    )

                    Text(
                        text = "Szachy",
                        color = stateManager.fontColor.value,
                        style = MaterialTheme.typography.titleLarge.copy(fontSize = 32.sp),
                        modifier = Modifier.padding(horizontal = 32.dp)
                    )

                    Image(
                        painter = painterResource(id = R.drawable.white_king), // Replace with your image resource
                        contentDescription = "Black King",
                        modifier = Modifier.size(imgSize),
                        contentScale = ContentScale.Crop
                    )
                }

                Spacer(modifier = Modifier.height(16.dp))

                val pieces = listOf(
                    Pair(R.drawable.black_queen, R.drawable.white_queen),
                    Pair(R.drawable.black_rook, R.drawable.white_rook),
                    Pair(R.drawable.black_bishop, R.drawable.white_bishop),
                    Pair(R.drawable.black_knight, R.drawable.white_knight),
                    Pair(R.drawable.black_pawn, R.drawable.white_pawn)
                )

                pieces.forEach { (black, white) ->
                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Image(
                            painter = painterResource(id = black),
                            contentDescription = null,
                            modifier = Modifier.size(imgSize)
                        )
                        Spacer(modifier = Modifier.width(16.dp))
                        Image(
                            painter = painterResource(id = white),
                            contentDescription = null,
                            modifier = Modifier.size(imgSize)
                        )
                    }
                    Spacer(modifier = Modifier.height(8.dp))
                }

                Spacer(modifier = Modifier.height(16.dp))

                Text(
                    text = "Szymon Nowak",
                    style = MaterialTheme.typography.bodyLarge,
                    color = stateManager.fontColor.value,
                    modifier = Modifier.padding(top = 8.dp)
                )
                Text(
                    text = "151862",
                    style = MaterialTheme.typography.bodyLarge,
                    color = stateManager.fontColor.value,
                    modifier = Modifier.padding(top = 4.dp)
                )
            }
            Spacer(modifier = Modifier.height(16.dp))
            NavBar(navController = navController)
        }
    }
}