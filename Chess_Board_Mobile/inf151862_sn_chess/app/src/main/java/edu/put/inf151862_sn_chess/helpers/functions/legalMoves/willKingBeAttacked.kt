package edu.put.inf151862_sn_chess.helpers.functions.legalMoves

import kotlin.math.abs

fun willKingBeAttacked(chessBoard: List<MutableList<Int>>, currentPos: Pair<Int, Int>, futurePos: Pair<Int, Int>): Boolean {
    val (currentX, currentY) = currentPos
    val (futureX, futureY) = futurePos

    // Store the piece at the future position temporarily
    val tempPiece = chessBoard[futureY][futureX]

    // Simulate the move by updating the chess board
    chessBoard[futureY][futureX] = chessBoard[currentY][currentX]
    chessBoard[currentY][currentX] = 0

    // Find the king's position
    val kingPos = findKingPosition(chessBoard, tempPiece)

    // Check if the king is under attack
    val isUnderAttack = isKingUnderAttack(chessBoard, kingPos, tempPiece)

    // Restore the original state of the chess board
    chessBoard[currentY][currentX] = chessBoard[futureY][futureX]
    chessBoard[futureY][futureX] = tempPiece

    return isUnderAttack
}

fun findKingPosition(chessBoard: List<List<Int>>, kingPiece: Int): Pair<Int, Int> {
    for (y in chessBoard.indices) {
        for (x in chessBoard[y].indices) {
            if (chessBoard[y][x] == kingPiece) {
                return Pair(x, y)
            }
        }
    }
    return Pair(-1, -1) // King not found (shouldn't happen in a legal chess position)
}

fun isKingUnderAttack(chessBoard: List<List<Int>>, kingPos: Pair<Int, Int>, kingPiece: Int): Boolean {
    val (kingX, kingY) = kingPos
    val opponentColor = if (kingPiece > 0) -1 else 1

    // Possible directions for opponent pieces to attack the king
    val directions = listOf(
        Pair(1, 0), Pair(-1, 0), Pair(0, 1), Pair(0, -1), // Rook-like moves
        Pair(1, 1), Pair(1, -1), Pair(-1, 1), Pair(-1, -1) // Bishop-like moves
    )

    // Check if any opponent piece can attack the king
    for ((dx, dy) in directions) {
        val (nextX, nextY) = Pair(kingX + dx, kingY + dy)
        if (nextX in 0..7 && nextY in 0..7 && chessBoard[nextY][nextX] * opponentColor < 0) {
            val piece = abs(chessBoard[nextY][nextX])
            if (piece == 1 || piece == 6) { // Pawn or king can attack only if they are adjacent
                return true
            }
            if (piece == 2 && abs(dx) + abs(dy) == 3) { // Knight can attack in L-shaped moves
                return true
            }
            if (piece == 3 || piece == 5) { // Rook or queen can attack horizontally or vertically
                if (dx == 0 || dy == 0) {
                    return true
                }
            }
            if (piece == 4 || piece == 5) { // Bishop or queen can attack diagonally
                if (abs(dx) == abs(dy)) {
                    return true
                }
            }
        }
    }

    return false
}