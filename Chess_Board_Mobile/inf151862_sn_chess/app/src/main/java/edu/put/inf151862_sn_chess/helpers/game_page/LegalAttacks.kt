package edu.put.inf151862_sn_chess.helpers.game_page

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import edu.put.inf151862_sn_chess.R
import edu.put.inf151862_sn_chess.helpers.functions.legalMoves.isValidPosition
import edu.put.inf151862_sn_chess.navigation.StateManager

@Composable
fun LegalAttacks(stateManager: StateManager, boxSize: Dp) {
    val pieceSize = boxSize / 8

    stateManager.legalAttacks.value.forEach { move ->
        val x = move[0]
        val y = move[1]

        if (x != null && y != null && isValidPosition(x, y)) {

            Image(
                painter = painterResource(id = R.drawable.attacked),
                contentDescription = "Legal Move",
                modifier = Modifier
                    .size(pieceSize*0.7f)
                    .offset((x * pieceSize.value + pieceSize.value * 0.15f).dp,
                        (y * pieceSize.value + pieceSize.value * 0.15f).dp)
                    .clickable {
                        stateManager.whiteOnMove.value = !stateManager.whiteOnMove.value
                        val chosenX = stateManager.chosenPiece.value[0]
                        val chosenY = stateManager.chosenPiece.value[1]

                        if (chosenX != null && chosenY != null) {
                            stateManager.chessBoard.value[y][x] = stateManager.chessBoard.value[chosenY][chosenX]
                            stateManager.chessBoard.value[chosenY][chosenX] = 0

                            stateManager.legalMoves.value = arrayOf(arrayOf<Int?>(null, null))
                            stateManager.legalAttacks.value = arrayOf(arrayOf<Int?>(null, null))

                            stateManager.chosenPiece.value = arrayOf(null, null)
                        }
                    }
            )
        }
    }
}