package edu.put.inf151862_sn_chess.helpers.functions

import androidx.compose.runtime.toMutableStateList
import edu.put.inf151862_sn_chess.navigation.StateManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

fun loadGame(stateManager: StateManager) {
    GlobalScope.launch{
        val chessBoardString = stateManager.db!!.myDbItemDao.getMyDbBoard()
        val newChessBoard = stringToChessBoard(chessBoardString)

        stateManager.chessBoard.value = newChessBoard.map { it.toMutableStateList() }
        stateManager.whiteOnMove.value = stateManager.db!!.myDbItemDao.getMyDbWhiteOnMMove()
    }
}

fun stringToChessBoard(chessBoardString: String): List<MutableList<Int>> {
    return chessBoardString.lines().map { line ->
        line.split(" ").map { it.toInt() }.toMutableList()
    }
}