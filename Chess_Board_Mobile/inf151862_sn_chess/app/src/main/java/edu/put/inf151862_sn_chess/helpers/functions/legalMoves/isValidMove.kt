package edu.put.inf151862_sn_chess.helpers.functions.legalMoves

fun isValidMove(x: Int, y: Int, chessBoard: List<MutableList<Int>>): Boolean {
    return x in 0..7 && y in 0..7 && chessBoard[y][x] == 0
}