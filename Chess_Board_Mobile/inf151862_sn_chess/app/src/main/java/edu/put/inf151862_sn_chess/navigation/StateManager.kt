package edu.put.inf151862_sn_chess.navigation

import androidx.compose.runtime.getValue
import androidx.compose.runtime.*
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.IntSize
import androidx.lifecycle.ViewModel
import edu.put.inf151862_sn_chess.helpers.RoomDb

class StateManager: ViewModel(){
    private val initialBoard: List<MutableList<Int>> = listOf(
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0),
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0),
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0),
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0),
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0),
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0),
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0),
        mutableListOf(0, 0, 0, 0, 0, 0, 0, 0)
    )
    //game state
    var chessBoard = mutableStateOf( initialBoard.map { row ->
        row.toMutableStateList()
    })
    val chosenPiece = mutableStateOf(arrayOf<Int?>(null, null))
    var legalMoves =  mutableStateOf(arrayOf(arrayOf<Int?>(null, null)))
    var legalAttacks = mutableStateOf(arrayOf(arrayOf<Int?>(null, null)))

    val whiteOnMove = mutableStateOf<Boolean>(true)

    //dark mode state
    val fontColor = mutableStateOf(Color.Black)
    val brightBackgroundColor = mutableStateOf(Color.White)
    val darkBackgroundColor = mutableStateOf(Color.Gray)
    val mediumBackgroundColor = mutableStateOf(Color.LightGray)
    var darkModeButton by mutableStateOf(false)

    //database
    var db: RoomDb? = null
    fun init(database: RoomDb){
        db = database
    }
}