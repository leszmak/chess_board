package edu.put.inf151862_sn_chess.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import edu.put.inf151862_sn_chess.helpers.RoomDb
import edu.put.inf151862_sn_chess.pages.Animation
import edu.put.inf151862_sn_chess.pages.GamePage
import edu.put.inf151862_sn_chess.pages.MainPage
import edu.put.inf151862_sn_chess.pages.SettingsPage
import edu.put.inf151862_sn_chess.pages.TabletAnimation
import edu.put.inf151862_sn_chess.pages.TabletGamePage
import edu.put.inf151862_sn_chess.pages.TabletMainPage
import edu.put.inf151862_sn_chess.pages.TabletSettingsPage

@Composable
fun Navigation(db: RoomDb) {
    var currentRoute by rememberSaveable { mutableStateOf(Screen.Animation.route) }
    val navController = rememberNavController()
    val stateManager = viewModel<StateManager>()
    stateManager.init(db)
    NavHost(navController = navController, startDestination = currentRoute) {
        composable(route = Screen.Animation.route) {
            Animation(navController)
        }
        composable(route = Screen.Settings.route) {
            SettingsPage(navController = navController, stateManager = stateManager)
        }
        composable(route = Screen.GamePage.route) {
            GamePage(navController = navController,  stateManager = stateManager)
        }
        composable(route = Screen.MainPage.route) {
            MainPage(navController = navController, stateManager = stateManager)
        }
        composable(route = Screen.TabletMainPage.route) {
            TabletMainPage(navController = navController, stateManager = stateManager)
        }
        composable(route = Screen.TabletGamePage.route) {
            TabletGamePage(navController = navController, stateManager = stateManager)
        }
        composable(route = Screen.TabletSettingsPage.route) {
            TabletSettingsPage(navController = navController, stateManager = stateManager)
        }
        composable(route = Screen.TabletAnimation.route) {
            TabletAnimation(navController)
        }
    }
}