package edu.put.inf151862_sn_chess.logic

import edu.put.inf151862_sn_chess.helpers.functions.legalMoves.isValidAttack
import edu.put.inf151862_sn_chess.helpers.functions.legalMoves.isValidMove
import edu.put.inf151862_sn_chess.navigation.StateManager

fun rook(stateManager: StateManager) {
    val x = stateManager.chosenPiece.value[0]
    val y = stateManager.chosenPiece.value[1]
    val chessBoard = stateManager.chessBoard.value

    var res = arrayOf(arrayOf<Int?>(null, null)) // Initialize legal moves array
    var res2 = arrayOf(arrayOf<Int?>(null, null)) // Initialize legal attacks array

    if (x == null || y == null) return // Exit if chosen piece position is null

    // Possible rook moves directions (horizontal and vertical)
    val directions = listOf(
        Pair(1, 0), Pair(-1, 0), Pair(0, 1), Pair(0, -1)
    )

    for (direction in directions) {
        var newX = x
        var newY = y

        while (true) {
            newX += direction.first
            newY += direction.second

            if (!isValidMove(newX, newY, chessBoard) && !isValidAttack(newX, newY, chessBoard, stateManager.whiteOnMove.value)) {
                break
            }

            if (isValidMove(newX, newY, chessBoard)) {
                res += arrayOf(newX, newY)
            }
            if (isValidAttack(newX, newY, chessBoard, stateManager.whiteOnMove.value)) {
                res2 += arrayOf(newX, newY)
                break
            }
        }
    }

    stateManager.legalMoves.value = res
    stateManager.legalAttacks.value = res2
}