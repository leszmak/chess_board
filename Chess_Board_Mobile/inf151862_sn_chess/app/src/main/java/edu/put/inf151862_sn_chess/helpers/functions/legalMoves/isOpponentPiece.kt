package edu.put.inf151862_sn_chess.helpers.functions.legalMoves

import edu.put.inf151862_sn_chess.navigation.StateManager

fun isOpponentPiece(x: Int, y: Int, piece: Int, stateManager: StateManager): Boolean {
    // Assuming positive numbers are white pieces and negative numbers are black pieces
    return (piece > 0 && stateManager.chessBoard.value[y][x] < 0) || (piece < 0 && stateManager.chessBoard.value[y][x] > 0)
}