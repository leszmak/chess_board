package edu.put.inf151862_sn_chess.helpers.game_page

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.offset
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import edu.put.inf151862_sn_chess.R
import edu.put.inf151862_sn_chess.navigation.StateManager
import androidx.compose.foundation.layout.size
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp

@Composable
fun ChosenPiece(stateManager: StateManager, boxSize: Dp) {
    val pieceSize = boxSize / 8
    val chosenPiece = stateManager.chosenPiece.value

    chosenPiece[0]?.let { row ->
        chosenPiece[1]?.let { col ->
            Image(
                painter = painterResource(id = R.drawable.chosen),
                contentDescription = "Chosen Piece",
                modifier = Modifier
                    .size(pieceSize * 0.7f)
                    .offset((row * pieceSize.value + pieceSize.value * 0.15f).dp,
                        (col * pieceSize.value + pieceSize.value * 0.15f).dp),
                contentScale = ContentScale.Fit
            )
        }
    }
}