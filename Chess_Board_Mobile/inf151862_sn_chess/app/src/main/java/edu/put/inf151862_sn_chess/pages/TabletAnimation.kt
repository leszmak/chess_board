package edu.put.inf151862_sn_chess.pages

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import edu.put.inf151862_sn_chess.R
import edu.put.inf151862_sn_chess.navigation.Screen
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Composable
fun TabletAnimation(navController: NavController) {
    var animationState by remember { mutableStateOf(TabletAnimationState.INITIAL) }
    var screenWidth by remember { mutableStateOf(0f) }
    val scope = rememberCoroutineScope()

    BoxWithConstraints(modifier = Modifier.fillMaxSize()) {
        if (maxWidth < maxHeight) navController.navigate(Screen.Animation.route)
        val boxSize = maxHeight
        val imgSize = boxSize / 5 // Adjusted size for better fit
        val blackOffsets = remember { List(6) { Animatable(-2000f) } }
        val whiteOffsets = remember { List(6) { Animatable(2000f) } }

        LaunchedEffect(animationState) {
            when (animationState) {
                TabletAnimationState.INITIAL -> {
                    blackOffsets.forEach { it.snapTo(-screenWidth) }
                    whiteOffsets.forEach { it.snapTo(screenWidth) }
                    animationState = TabletAnimationState.MOVE_PIECES
                }

                TabletAnimationState.MOVE_PIECES -> {
                    blackOffsets.forEachIndexed { index, animatable ->
                        scope.launch {
                            animatable.animateTo(
                                targetValue = 0f,
                                animationSpec = tween(durationMillis = 1000)
                            )
                        }
                    }
                    whiteOffsets.forEachIndexed { index, animatable ->
                        scope.launch {
                            animatable.animateTo(
                                targetValue = 0f,
                                animationSpec = tween(durationMillis = 1000)
                            )
                        }
                    }
                    delay(1000) // Wait for animations to complete
                    animationState = TabletAnimationState.WAIT
                }

                TabletAnimationState.WAIT -> {
                    delay(1000)
                    navController.navigate(Screen.MainPage.route)
                }
            }
        }

        BoxWithConstraints(
            modifier = Modifier.fillMaxSize()
        ) {
            screenWidth = maxWidth.value

            val blackPieces = listOf(
                R.drawable.black_king,
                R.drawable.black_queen,
                R.drawable.black_rook,
                R.drawable.black_bishop,
                R.drawable.black_knight,
                R.drawable.black_pawn
            )

            val whitePieces = listOf(
                R.drawable.white_king,
                R.drawable.white_queen,
                R.drawable.white_rook,
                R.drawable.white_bishop,
                R.drawable.white_knight,
                R.drawable.white_pawn
            )

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center,
                modifier = Modifier.fillMaxSize()
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    blackPieces.forEachIndexed { index, piece ->
                        Image(
                            painter = painterResource(id = piece),
                            contentDescription = null,
                            modifier = Modifier
                                .size(imgSize)
                                .graphicsLayer(translationX = blackOffsets[index].value),
                            contentScale = ContentScale.Fit
                        )
                    }
                }

                Spacer(modifier = Modifier.height(16.dp)) // Space between rows

                Row(
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    whitePieces.forEachIndexed { index, piece ->
                        Image(
                            painter = painterResource(id = piece),
                            contentDescription = null,
                            modifier = Modifier
                                .size(imgSize)
                                .graphicsLayer(translationX = whiteOffsets[index].value),
                            contentScale = ContentScale.Fit
                        )
                    }
                }
            }
        }
    }
}

enum class TabletAnimationState {
    INITIAL, MOVE_PIECES, WAIT
}