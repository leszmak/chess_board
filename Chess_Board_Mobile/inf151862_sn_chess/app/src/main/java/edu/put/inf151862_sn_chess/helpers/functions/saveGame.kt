package edu.put.inf151862_sn_chess.helpers.functions

import androidx.compose.runtime.State
import edu.put.inf151862_sn_chess.helpers.MyDbGame
import edu.put.inf151862_sn_chess.navigation.StateManager
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

fun saveGame(stateManager: StateManager) {
    val chessBoardString = chessBoardToString(stateManager.chessBoard)

    val data = MyDbGame(board = chessBoardString, whiteOnMove = stateManager.whiteOnMove.value)
    GlobalScope.launch {
        stateManager.db!!.myDbItemDao.clear()
        stateManager.db!!.myDbItemDao.upsertMyDbItem(data)
    }

}

fun chessBoardToString(chessBoard: State<List<MutableList<Int>>>): String {
    return chessBoard.value.joinToString(separator = "\n") { row ->
        row.joinToString(separator = " ")
    }
}